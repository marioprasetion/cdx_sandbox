package com.msi.cdx.repository;

import com.msi.cdx.model.internal.Customer;
import com.msi.cdx.model.internal.PhoneMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface PhoneMappingRepository extends JpaRepository<PhoneMapping, Long> {
    List<PhoneMapping> findAllByCustomer(Customer customer);
    List<PhoneMapping> findAllByCustomerAndPhoneNumber(Customer customer, String phoneNumber);
}
