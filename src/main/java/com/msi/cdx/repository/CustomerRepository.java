package com.msi.cdx.repository;

import com.msi.cdx.model.internal.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {
//    List<Customer> findAllByNameContainingOrCifContainingOrPhoneNumberOrEmailContainingOrFacebookAccountContainingOrTwitterAccountContainingOrWhatsappAccountContainingOrTelegramAccountContainingOrLineAccountContainingOrInstagramAccountContaining(
//            String name, String cif, String phoneNo, String Email, String facebook,
//            String twitter, String whatsapp, String telegram, String line,
//            String instagram, Pageable pageable);
    Customer findByPhoneNumber(String phoneNumber);
    List<Customer> findAllByPhoneNumberContaining(String phoneNumber);
    List<Customer> findAllByPhoneNumberContainingAndPhoneNumberNot(String phoneNumber, String currentPhonenUmber);
}

