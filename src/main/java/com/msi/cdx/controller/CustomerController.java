package com.msi.cdx.controller;

import com.msi.cdx.model.internal.Customer;
import com.msi.cdx.repository.CustomerRepository;
import com.msi.cdx.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = {"/customer"})
@Slf4j
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerService customerService;

//    @RequestMapping(value = {""}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> addCustomer(@RequestBody Customer customer) {
//        try {
//            return ResponseEntity.ok().body(customerService.save(customer));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }
//
//    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> updateCustomer(@PathVariable(value = "id") Long id,
//                                            @RequestBody Customer newCustomer) {
//        try {
//            Customer customer = customerRepository.findOne(id);
//            if(customer != null){
//                return ResponseEntity.ok().body(customerService.update(customer,newCustomer));
//            }
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }

    @RequestMapping(value = {""}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomer(@RequestParam("agentid") String agentId,
                                         @RequestParam("agentname") String agentName,
                                         @RequestParam("phone") String phoneNumber) {
        try {
            Customer customer = customerService.getCustomerByPhone(phoneNumber);
            if(customer != null){
                return ResponseEntity.ok().body(customer);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("getCustomer() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/search-journey"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> searchJourney(@RequestBody Map<String,Object> body) {
        try {
            String agentId = body.get("agent_name").toString();
            String agentName = body.get("agent_id").toString();
            String phoneNumber = body.get("phone_number").toString();
            String email = body.get("email").toString();
            Customer customer = customerRepository.findByPhoneNumber(phoneNumber);
            if(customer != null){
                return ResponseEntity.ok().body(customerService.getActivities(agentId,agentName,customer,email));
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("searchJourney() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/conversation"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> searchJourney(@RequestParam("agentid") String agentId,
                                           @RequestParam("agentname") String agentName,
                                           @RequestParam("conversationid") String conversationId,
                                           @RequestParam("phonenumber") String phoneNumber,
                                           @RequestParam("searchphonenumber") String searchPhoneNumber,
                                           @RequestParam("source") String source) {
        try {
            return ResponseEntity.ok().body(customerService.getConversation(agentId,agentName,conversationId,source,
                    phoneNumber, searchPhoneNumber));
        } catch (Exception e) {
            log.error("searchJourney() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/content"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmailContent(@RequestParam("id") String id) {
        try {
            return ResponseEntity.ok().body(customerService.getEmailContent(id));
        } catch (Exception e) {
            log.error("getEmailContent() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/add-main-customer"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addMainCustomer(@RequestBody Map<String,Object> body) {
        try {
            String phoneNumber = body.get("phone_number").toString();
            String agentId = body.get("agent_name").toString();
            String agentName = body.get("agent_id").toString();
            String callKey = body.get("call_key").toString();
            String callKeyDay = body.get("call_key_day").toString();
            return ResponseEntity.ok().body(customerService.addMainCustomer(phoneNumber, agentId, agentName, callKey, callKeyDay));
        } catch (Exception e) {
            log.error("addMainCustomer() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/add-phone-mapping"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> AddPhoneMapping(@RequestBody Map<String,Object> body) {
        try {
            String customerId = body.get("customer_id").toString();
            String phoneNumber = body.get("phone_number").toString();
            String agentId = body.get("agent_name").toString();
            String agentName = body.get("agent_id").toString();
            String callKey = body.get("call_key").toString();
            String callKeyDay = body.get("call_key_day").toString();
            Customer customer = customerRepository.getOne(Long.parseLong(customerId));
            if(customer != null){
                return ResponseEntity.ok().body(customerService.addPhoneMapping(customer, phoneNumber,agentId, agentName, callKey, callKeyDay));
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("AddPhoneMapping() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/replace-customer"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> replaceCustomer(@RequestBody Map<String,Object> body) {
        try {
            String customerId = body.get("customer_id").toString();
            String newPhoneNumber = body.get("new_phone_number").toString();
            String agentId = body.get("agent_name").toString();
            String agentName = body.get("agent_id").toString();
            String callKey = body.get("call_key").toString();
            String callKeyDay = body.get("call_key_day").toString();
            Customer customer = customerRepository.getOne(Long.parseLong(customerId));
            if(customer != null){
                return ResponseEntity.ok().body(customerService.replaceCustomer(customer, newPhoneNumber,agentId, agentName, callKey, callKeyDay));
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("replaceCustomer() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/phone"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPhone(@RequestParam("agentid") String agentId,
                                      @RequestParam("agentname") String agentName,
                                      @RequestParam("phonenumber") String phoneNumber,
                                      @RequestParam("currentphonenumber") String currentPhoneNumber) {
        try {
            List<Customer> customer = customerRepository.findAllByPhoneNumberContainingAndPhoneNumberNot(phoneNumber, currentPhoneNumber);
            if(customer != null){
                return ResponseEntity.ok().body(customer);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            log.error("getPhone() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/remove-phones"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removePhones(@RequestBody Long[] ids) {
        try {
            customerService.removePhones(ids);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error("AddPhoneMapping() ",e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }



//    @RequestMapping(value = {"/unlink"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> unlinkPhone(@RequestBody Customer customer) {
//        try {
//            return ResponseEntity.ok().body(customerService.save(customer));
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }

//    @RequestMapping(value = {"/linking"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> linkingPhone(@RequestBody Map<String,Object> body) {
//        try {
//            String phoneId = body.get("phone_id").toString();
//            String customerId = body.get("customer_id").toString();
//            String agentId = body.get("agent_name").toString();
//            String agentName = body.get("agent_id").toString();
//            return ResponseEntity.ok().body(customerService.linking(agentId, agentName, Long.parseLong(customerId, 10),Long.parseLong(phoneId,10)));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }
//
//    @RequestMapping(value = {"/remove-phone"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> removePhone(@RequestBody Map<String,Object> body) {
//        try {
//            String phoneId = body.get("phone_id").toString();
//            String customerId = body.get("customer_id").toString();
//            String agentId = body.get("agent_name").toString();
//            String agentName = body.get("agent_id").toString();
//            return ResponseEntity.ok().body(customerService.removePhone(agentId, agentName, Long.parseLong(customerId, 10),Long.parseLong(phoneId,10)));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }
//
//    @RequestMapping(value = {"/add-main-customer"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> addPhone(@RequestBody Map<String,Object> body) {
//        try {
//            String phoneId = body.get("phone_number").toString();
//            String agentId = body.get("agent_name").toString();
//            String agentName = body.get("agent_id").toString();
//            return ResponseEntity.ok().body(customerService.removePhone(agentId, agentName, Long.parseLong(customerId, 10),Long.parseLong(phoneId,10)));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }

}