package com.msi.cdx.controller;

import com.msi.cdx.model.pcce.ActivityHub;
import com.msi.cdx.model.internal.EmailContent;
import com.msi.cdx.repositoryHub.ActivityRepository;
import com.msi.cdx.service.JourneyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = {"/journey"})
@Slf4j
public class JourneyController {

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    JourneyService journeyService;

    @RequestMapping(value = {"/test"}, method = RequestMethod.GET)
    public ResponseEntity<?> test() {
        return ResponseEntity.ok().body("test v2");
    }

    @RequestMapping(value = {"/call"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCallHistory(@RequestParam("phone") String phone,
                                            @RequestParam(value = "callKey", required=false) String callKey,
                                            @RequestParam(value = "callKeyDay", required=false) String callKeyDay,
                                            @RequestParam(value = "lastCallKey", required=false) String lastCallKey,
                                            @RequestParam(value = "lastCallKeyDay", required=false) String lastCallKeyDay,
                                            @RequestParam(value = "account", required=false) String account) {
        try {
            if(account != null){
                log.info("getCallHistory() main Account :: callKey and callKeyDay "+callKey+"-"+callKeyDay);
                List<ActivityHub> activityList = activityRepository.getHistoryCallByCallKeyIdGreatherThan(phone,
                        callKeyDay+""+callKey);
                return ResponseEntity.ok().body(activityList);
            }else{
                if (lastCallKeyDay == null && lastCallKey == null){
                    log.info("getCallHistory() :: callKey and callKeyDay "+callKey+"-"+callKeyDay);
                    List<ActivityHub> activityList = activityRepository.getHistoryCallByCallKeyDayAndCallKey(phone,callKeyDay,
                            callKey);
                    return ResponseEntity.ok().body(activityList);
                }
                else{
                    log.info("getCallHistory() :: callKey and callKeyDay "+callKey+"-"+callKeyDay);
                    log.info("getCallHistory() :: lastCallKey and lastCallKeyDay "+lastCallKey+"-"+lastCallKeyDay);
                    List<ActivityHub> activityList =
                            activityRepository.getHistoryCallBetweenCallKeyIdAndLastCallKeyId(phone,
                                    callKeyDay+""+callKey, lastCallKeyDay+""+lastCallKey);
                    return ResponseEntity.ok().body(activityList);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/email"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmailHistory(@RequestParam("email") String email) {
        try {
       //     List<Activity> activities = journeyService.getEmailHistory(email);
            List<ActivityHub> activities = journeyService.getEmailHistoryWoContent(email);
     //       List<Activity> activities = new ArrayList<>();
            return ResponseEntity.ok().body(activities);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }
    @RequestMapping(value = {"/social-media"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSocialHistory(@RequestParam("email") String email) {
        try {
            List<ActivityHub> activities = journeyService.getSocialHistory(email);
        //    List<Activity> activities = new ArrayList<>();
            return ResponseEntity.ok().body(activities);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }

    @RequestMapping(value = {"/email/{conversationId}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEmailHistoryDetails(@PathVariable(value = "conversationId") String id) {
        try {
            List<ActivityHub> activities = journeyService.getEmailDetails(id);
            return ResponseEntity.ok().body(activities);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }
    @RequestMapping(value = {"/content"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getContent(@RequestParam("id") String id) {
        try {
            EmailContent emailContent = journeyService.getEmailContent(id);
            return ResponseEntity.ok().body(emailContent);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }
    @RequestMapping(value = {"/social-media/{conversationId}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSocialHistoryDetails(@PathVariable(value = "conversationId") String id) {
        try {
            List<ActivityHub> activities = journeyService.getSocialDetails(id);
            return ResponseEntity.ok().body(activities);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }


}
