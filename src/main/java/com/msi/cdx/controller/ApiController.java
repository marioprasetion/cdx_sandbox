package com.msi.cdx.controller;


import com.msi.cdx.model.internal.Activity;
import com.msi.cdx.model.internal.Customer;
import com.msi.cdx.model.internal.Journey;
import com.msi.cdx.repository.CustomerRepository;
import com.msi.cdx.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = {"/api"})
@Slf4j
public class ApiController {

    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping(value = {"/journey"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addCustomer(@RequestParam("agentid") String agentId,
                                         @RequestParam("agentname") String agentName,
                                         @RequestParam(value = "phoneNumber", required=false) String phoneNumber,
                                         @RequestParam(value ="email", required=false) String email ) {
        try {
            List<Activity> activities = new ArrayList<>();
            if(phoneNumber != null){
                Customer customer = customerRepository.findByPhoneNumber(phoneNumber);
                if(customer != null){
                    Journey journey = customerService.getActivities(agentId,agentName,customer,email);
                    Collections.sort(journey.getActivities());
                    return ResponseEntity.ok().body(activities);
                }
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
        }
    }
}
