package com.msi.cdx.controller;

import com.msi.cdx.model.internal.Activity;
import com.msi.cdx.model.internal.Customer;
import com.msi.cdx.model.internal.Journey;
import com.msi.cdx.repository.CustomerRepository;
import com.msi.cdx.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"/"})
@Slf4j
public class ViewController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = {"add-customer"}, method = RequestMethod.GET)
    public String addCustomer(@RequestParam("agentid") String agentId, @RequestParam("agentname") String agentName, Model model) {
        model.addAttribute("agentid",agentId);
        model.addAttribute("agentname",agentName);
        return "add-customer";
    }

    @RequestMapping(value = {"customer-form/{id}"}, method = RequestMethod.GET)
    public String customerForm(@PathVariable(value = "id") Long id,
                               @RequestParam("agentid") String agentId,
                               @RequestParam("agentname") String agentName, Model model) {
        Customer customer = customerRepository.findOne(id);
        model.addAttribute("customer",customer);
        model.addAttribute("agentid",agentId);
        model.addAttribute("agentname",agentName);
        return "customer-form";
    }

    @RequestMapping(value = {"search"}, method = RequestMethod.GET)
    public String search(@RequestParam("agentid") String agentId,
                         @RequestParam("agentname") String agentName,
                         @RequestParam("q") String q,Model model) {
        Pageable paging = new PageRequest(0, 50);
//        List<Customer> customers = customerRepository.findAllByNameContainingOrCifContainingOrPhoneNumberOrEmailContainingOrFacebookAccountContainingOrTwitterAccountContainingOrWhatsappAccountContainingOrTelegramAccountContainingOrLineAccountContainingOrInstagramAccountContaining(q, q, q,q,q,q,q,q,q,q,paging);
//        model.addAttribute("customers",customers);
        model.addAttribute("agentid",agentId);
        model.addAttribute("agentname",agentName);
        return "search";
    }

    @RequestMapping(value = {"journey"}, method = RequestMethod.GET)
    public String journey( @RequestParam("agentid") String agentId,
                           @RequestParam("agentname") String agentName,
                           @RequestParam(value = "phoneNumber", required=false) String phoneNumber,
                           @RequestParam(value = "callKey", required=false) String callKey,
                           @RequestParam(value = "callKeyDay", required=false) String callKeyDay,
                           @RequestParam(value = "searchPhoneNumber", required=false) String searchPhoneNumber,
                           @RequestParam(value ="email", required=false, defaultValue = "") String email ,
                           Model model, HttpServletRequest request) throws Exception {
        List<Activity> activities = new ArrayList<>();
        Journey journey = new Journey();
        boolean isMainAccount = false;
        if(phoneNumber != null && phoneNumber.length() > 0){
            Customer customer = customerRepository.findByPhoneNumber(phoneNumber);
            if(customer != null){
                isMainAccount = true;
                activities.addAll(customerService.getActivitiesByCustomer(agentId,agentName,customer));
                model.addAttribute("customer", customer);
            }
        }
        if(searchPhoneNumber != null && searchPhoneNumber.length() > 0 && !phoneNumber.equals(searchPhoneNumber)){
            log.info("journey() searchPhoneNumber ");
            Customer customer = customerRepository.findByPhoneNumber(searchPhoneNumber);
            if(customer != null){
                activities.addAll(customerService.getActivitiesByCustomer(agentId,agentName,customer));
                model.addAttribute("searchCustomer", customer);
            }
        }
        if(email != null && email.length() > 0 ){
            activities.addAll(customerService.getConversationByEmail(agentId, agentName, email));
        }
        model.addAttribute("isMainAccount", isMainAccount);
        model.addAttribute("activities", activities);
        model.addAttribute("email",email);
        model.addAttribute("phoneNumber",phoneNumber);
        model.addAttribute("callKey",callKey);
        model.addAttribute("callKeyDay",callKeyDay);
        model.addAttribute("searchPhoneNumber",searchPhoneNumber);
        model.addAttribute("agentid",agentId);
        model.addAttribute("agentname",agentName);
        return "journey";
    }

}
