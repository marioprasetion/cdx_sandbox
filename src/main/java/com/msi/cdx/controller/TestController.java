package com.msi.cdx.controller;

import com.msi.cdx.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = {"/test"})
public class TestController {

    @Autowired
    CustomerRepository customerRepository;

//    @RequestMapping(value = {"/customer"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> addCustomer() {
//        try {
//            Customer customer = new Customer();
//            customer.setName("1");
//            customer.setAddress("address1");
//            customer.setCreatedIdBy("id1");
//            customer.setCreatedNameBy("name1");
//            customer.setModifiedIdBy("id1");
//            customer.setModifiedNameBy("name1");
//            customer.setModifiedAt(new Timestamp(System.currentTimeMillis()));
//            customerRepository.save(customer);
//            return ResponseEntity.ok().body(customer);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }
//
//    @RequestMapping(value = {"/customer"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> updateCustomer() {
//        try {
//            Customer customer = customerRepository.findOne(1L);
//            customer.setName("2");
//            customer.setAddress("address2");
//            customer.setCreatedIdBy("id2");
//            customer.setCreatedNameBy("name2");
//            customer.setModifiedIdBy("id2");
//            customer.setModifiedNameBy("name2");
//            customer.setModifiedAt(new Timestamp(System.currentTimeMillis()));
//            customerRepository.save(customer);
//            return ResponseEntity.ok().body(customer);
//        } catch (Exception e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Error(e.getMessage()));
//        }
//    }

}
