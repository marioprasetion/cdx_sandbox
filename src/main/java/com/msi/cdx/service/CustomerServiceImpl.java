package com.msi.cdx.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msi.cdx.model.internal.*;
import com.msi.cdx.repository.CustomerRepository;
import com.msi.cdx.repository.PhoneMappingRepository;
import com.msi.cdx.util.SslUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;


    @Autowired
    PhoneMappingRepository phoneMappingRepository;

    @Value("${hub.address}")
    private String hubAddress;

    @Override
    public Customer save(Customer customer) throws Exception {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        customer.setModifiedAt(timestamp);
        customer.setCreatedAt(timestamp);
        return customerRepository.save(customer);
    }

    @Override
    public Journey getActivities(String agentId, String agentName, Customer customer, String email) throws Exception{
        List<Activity> allActivities = new ArrayList<>();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        List<String> phoneList = new ArrayList<>();
        List<Activity> activities = new ArrayList<>();
        Journey journey = new Journey();
        log.info("getActivities() "+email);
        journey.setCustomer(customer);
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        try {
            httpclient.start();
            List<PhoneMapping> phones = phoneMappingRepository.findAllByCustomer(customer);
            PhoneMapping phoneMapping = new PhoneMapping();
            phoneMapping.setPhoneNumber(customer.getPhoneNumber());
            phoneMapping.setCallKey(customer.getCallKey());
            phoneMapping.setCallKeyDay(customer.getCallKeyDay());
            phones.add(phoneMapping);
            for(PhoneMapping data : phones){
                final CountDownLatch latchCall = new CountDownLatch(1);
                final  HttpGet httpGetCall;
                if( data.getLastCallKey() != null && data.getLastCallKeyDay() != null)
                {
                    log.info("getActivities() masuk if ");
                    URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                    builder.setParameter("phone", data.getPhoneNumber())
                            .setParameter("callKey", data.getCallKey())
                            .setParameter("callKeyDay", ""+data.getCallKeyDay())
                            .setParameter("lastCallKey", ""+data.getLastCallKey())
                            .setParameter("lastCallKeyDay", ""+data.getLastCallKeyDay());
                    httpGetCall = new HttpGet(builder.build());
                }else if(data.getCustomer() == null){
                    log.info("getActivities() masuk else if ");
                    URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                    builder.setParameter("phone", data.getPhoneNumber())
                            .setParameter("account", "main")
                            .setParameter("callKey", data.getCallKey())
                            .setParameter("callKeyDay", ""+data.getCallKeyDay());
                    httpGetCall = new HttpGet(builder.build());
                }else {
                    log.info("getActivities() masuk else ");
                    httpGetCall = new HttpGet(hubAddress + "/journey/call?phone="+data.getPhoneNumber()+"&callKey="+data.getCallKey()+"&callKeyDay="+data.getCallKeyDay());
                }

                httpGetCall.setHeader("Content-type", "application/json");
                httpclient.execute(httpGetCall, new FutureCallback<HttpResponse>() {
                    public void completed(final HttpResponse response) {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + response.getStatusLine());
                        int statusCode = response.getStatusLine().getStatusCode();
                        String sResponse = "";
                        try {
                            sResponse = EntityUtils.toString(response.getEntity());
                            if (statusCode == 200) {
                                ObjectMapper mapper = new ObjectMapper();
                                List<Activity> activities = mapper.readValue(sResponse,
                                        new TypeReference<ArrayList<Activity>>() {});
                                allActivities.addAll(activities);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    public void failed(final Exception ex) {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + ex);
                    }

                    public void cancelled() {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + " httpGetCall cancelled");
                    }

                });
                latchCall.await();
            }
            final CountDownLatch latchSocmed = new CountDownLatch(1);
            final  HttpGet httpGetSocmed = new HttpGet(hubAddress + "/journey/social-media?email="+email);
            httpGetSocmed.setHeader("Content-type", "application/json");
            httpclient.execute(httpGetSocmed, new FutureCallback<HttpResponse>() {
                public void completed(final HttpResponse response) {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + "-> httpGetSocmed" + response.getStatusLine());
                    int statusCode = response.getStatusLine().getStatusCode();
                    String sResponse = "";
                    try {
                        sResponse = EntityUtils.toString(response.getEntity());
                        if (statusCode == 200) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Activity> activities = mapper.readValue(sResponse,
                                    new TypeReference<ArrayList<Activity>>() {});
                            allActivities.addAll(activities);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                public void failed(final Exception ex) {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + "-> httpGetSocmed" + ex);
                }

                public void cancelled() {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + " httpGetSocmed cancelled");
                }

            });
            latchSocmed.await();
            final CountDownLatch latchEmail = new CountDownLatch(1);
            final  HttpGet httpGetEmail = new HttpGet(hubAddress + "/journey/email?email="+email);
            httpGetEmail.setHeader("Content-type", "application/json");
            httpclient.execute(httpGetEmail, new FutureCallback<HttpResponse>() {
                public void completed(final HttpResponse response) {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + "-> httpGetEmail" + response.getStatusLine());
                    int statusCode = response.getStatusLine().getStatusCode();
                    String sResponse = "";
                    try {
                        sResponse = EntityUtils.toString(response.getEntity());
                        if (statusCode == 200) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Activity> activities = mapper.readValue(sResponse,
                                    new TypeReference<ArrayList<Activity>>() {});
                            allActivities.addAll(activities);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                public void failed(final Exception ex) {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + "-> httpGetEmail" + ex);
                }

                public void cancelled() {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + " httpGetEmail cancelled");
                }

            });
            latchEmail.await();


        } finally {
            httpclient.close();
        }
        journey.setActivities(allActivities);
        return journey;
    }

    @Override
    public List<Activity> getActivitiesByCustomer(String agentId, String agentName, Customer customer) throws Exception {
        List<Activity> activityList = new ArrayList<>();
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        try {
            httpclient.start();
            List<PhoneMapping> phones = phoneMappingRepository.findAllByCustomer(customer);
            PhoneMapping phoneMapping = new PhoneMapping();
            phoneMapping.setPhoneNumber(customer.getPhoneNumber());
            phoneMapping.setCallKey(customer.getCallKey());
            phoneMapping.setCallKeyDay(customer.getCallKeyDay());
            phones.add(phoneMapping);
            for(PhoneMapping data : phones){
                final CountDownLatch latchCall = new CountDownLatch(1);
                final  HttpGet httpGetCall;
                if( data.getLastCallKey() != null && data.getLastCallKeyDay() != null)
                {
                    log.info("getActivitiesByCustomer() masuk if ");
                    URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                    builder.setParameter("phone", data.getPhoneNumber())
                            .setParameter("callKey", data.getCallKey())
                            .setParameter("callKeyDay", ""+data.getCallKeyDay())
                            .setParameter("lastCallKey", ""+data.getLastCallKey())
                            .setParameter("lastCallKeyDay", ""+data.getLastCallKeyDay());
                    httpGetCall = new HttpGet(builder.build());
                }else if(data.getCustomer() == null){
                    log.info("getActivitiesByCustomer() masuk else if ");
                    URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                    builder.setParameter("phone", data.getPhoneNumber())
                            .setParameter("account", "main")
                            .setParameter("callKey", data.getCallKey())
                            .setParameter("callKeyDay", ""+data.getCallKeyDay());
                    httpGetCall = new HttpGet(builder.build());
                }else {
                    log.info("getActivitiesByCustomer() masuk else ");
                    httpGetCall = new HttpGet(hubAddress + "/journey/call?phone="+data.getPhoneNumber()+"&callKey="+data.getCallKey()+"&callKeyDay="+data.getCallKeyDay());
                }

                httpGetCall.setHeader("Content-type", "application/json");
                httpclient.execute(httpGetCall, new FutureCallback<HttpResponse>() {
                    public void completed(final HttpResponse response) {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + response.getStatusLine());
                        int statusCode = response.getStatusLine().getStatusCode();
                        String sResponse = "";
                        try {
                            sResponse = EntityUtils.toString(response.getEntity());
                            if (statusCode == 200) {
                                ObjectMapper mapper = new ObjectMapper();
                                List<Activity> activities = mapper.readValue(sResponse,
                                        new TypeReference<ArrayList<Activity>>() {});
                                activityList.addAll(activities);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    public void failed(final Exception ex) {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + ex);
                    }

                    public void cancelled() {
                        latchCall.countDown();
                        System.out.println(httpGetCall.getRequestLine() + " httpGetCall cancelled");
                    }

                });
                latchCall.await();
            }
        } finally {
            httpclient.close();
        }
        return activityList;
    }

    @Override
    public List<Activity> getConversation(String agentId, String agentName, String conversationId, String source,
                                          String phoneNumber, String searchPhonenumber) throws Exception {
        List<Activity> phoneActivities = new ArrayList<>();
        List<Activity> digitalActivities = new ArrayList<>();
        if(source.equals("phone")){
            Customer customer = customerRepository.findByPhoneNumber(phoneNumber);
            Customer searchCustomer = customerRepository.findByPhoneNumber(searchPhonenumber);
            List<PhoneMapping> phones = new ArrayList<>();
            if(customer != null){
                phones.addAll(phoneMappingRepository.findAllByCustomerAndPhoneNumber(customer, conversationId));
                if(customer.getPhoneNumber().equals(conversationId)){
                    PhoneMapping phoneMapping = new PhoneMapping();
                    phoneMapping.setPhoneNumber(customer.getPhoneNumber());
                    phoneMapping.setCallKey(customer.getCallKey());
                    phoneMapping.setCallKeyDay(customer.getCallKeyDay());
                    phones.add(phoneMapping);
                }
            }
            if(searchCustomer != null && !phoneNumber.equals(searchPhonenumber)){
                phones.addAll(phoneMappingRepository.findAllByCustomerAndPhoneNumber(searchCustomer, conversationId));
                if(searchCustomer.getPhoneNumber().equals(conversationId)){
                    PhoneMapping phoneMapping = new PhoneMapping();
                    phoneMapping.setPhoneNumber(searchCustomer.getPhoneNumber());
                    phoneMapping.setCallKey(searchCustomer.getCallKey());
                    phoneMapping.setCallKeyDay(searchCustomer.getCallKeyDay());
                    phones.add(phoneMapping);
                }
            }
            CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            try {
                httpclient.start();
                for(PhoneMapping data : phones) {
                    final CountDownLatch latchCall = new CountDownLatch(1);
                    final HttpGet httpGetCall;
                    if (data.getLastCallKey() != null && data.getLastCallKeyDay() != null) {
                        log.info("getActivities() masuk if ");
                        URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                        builder.setParameter("phone", data.getPhoneNumber())
                                .setParameter("callKey", data.getCallKey())
                                .setParameter("callKeyDay", "" + data.getCallKeyDay())
                                .setParameter("lastCallKey", "" + data.getLastCallKey())
                                .setParameter("lastCallKeyDay", "" + data.getLastCallKeyDay());
                        httpGetCall = new HttpGet(builder.build());
                    } else if (data.getCustomer() == null) {
                        log.info("getActivities() masuk else if ");
                        URIBuilder builder = new URIBuilder(hubAddress + "/journey/call");
                        builder.setParameter("phone", data.getPhoneNumber())
                                .setParameter("account", "main")
                                .setParameter("callKey", data.getCallKey())
                                .setParameter("callKeyDay", "" + data.getCallKeyDay());
                        httpGetCall = new HttpGet(builder.build());
                    } else {
                        log.info("getActivities() masuk else ");
                        httpGetCall = new HttpGet(hubAddress + "/journey/call?phone=" + data.getPhoneNumber() + "&callKey=" + data.getCallKey() + "&callKeyDay=" + data.getCallKeyDay());
                    }

                    httpGetCall.setHeader("Content-type", "application/json");
                    httpclient.execute(httpGetCall, new FutureCallback<HttpResponse>() {
                        public void completed(final HttpResponse response) {
                            latchCall.countDown();
                            System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + response.getStatusLine());
                            int statusCode = response.getStatusLine().getStatusCode();
                            String sResponse = "";
                            try {
                                sResponse = EntityUtils.toString(response.getEntity());
                                if (statusCode == 200) {
                                    ObjectMapper mapper = new ObjectMapper();
                                    List<Activity> activities = mapper.readValue(sResponse,
                                            new TypeReference<ArrayList<Activity>>() {
                                            });
                                    phoneActivities.addAll(activities);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        public void failed(final Exception ex) {
                            latchCall.countDown();
                            System.out.println(httpGetCall.getRequestLine() + "-> httpGetCall" + ex);
                        }

                        public void cancelled() {
                            latchCall.countDown();
                            System.out.println(httpGetCall.getRequestLine() + " httpGetCall cancelled");
                        }

                    });
                    latchCall.await();
                }
            }finally {
                httpclient.close();
            }
            Collections.reverse(phoneActivities);
            return phoneActivities;
        }else{
            String url = hubAddress+"/journey/social-media/"+conversationId;
            if(source.equals("email"))
                url = hubAddress+"/journey/email/"+conversationId;
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Content-type", "application/json");
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            String sResponse = EntityUtils.toString(response.getEntity());
            log.info("getConversation() :: " + url);
            log.info("getConversation() :: " + statusCode);
            log.info("getConversation() body :: " + sResponse);
            if (statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                digitalActivities = mapper.readValue(sResponse,
                        new TypeReference<ArrayList<Activity>>() {});
            }
            Collections.reverse(digitalActivities);
            return digitalActivities;
        }

    }

    @Override
    public Customer getCustomerByPhone(String phoneNumber) throws Exception {
        return customerRepository.findByPhoneNumber(phoneNumber);
    }

    private List<Activity>  getPhoneHistory(String phoneNumber) throws Exception{
        List<Activity> activities = new ArrayList<>();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress + "/journey/call?phone="+phoneNumber);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        String sResponse = EntityUtils.toString(response.getEntity());
        log.info("getEmailHistory() :: " + statusCode);
        log.info("getEmailHistory() body :: " + sResponse);
        if (statusCode == 200) {
            ObjectMapper mapper = new ObjectMapper();
            activities = mapper.readValue(sResponse,
                    new TypeReference<ArrayList<Activity>>() {});
        }
        return activities;
    }

    private List<Activity>  getSocmedHistory(String email) throws Exception{
        List<Activity> activities = new ArrayList<>();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress + "/journey/social-media?email="+email);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        String sResponse = EntityUtils.toString(response.getEntity());
        log.info("getSocmedHistory() :: " + statusCode);
        log.info("getSocmedHistory() body :: " + sResponse);
        if (statusCode == 200) {
            ObjectMapper mapper = new ObjectMapper();
            activities = mapper.readValue(sResponse,
                    new TypeReference<ArrayList<Activity>>() {});
        }
        return activities;
    }
    private List<Activity>  getEmailHistory(String email) throws Exception{
        List<Activity> activities = new ArrayList<>();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress + "/journey/email?email="+email);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        String sResponse = EntityUtils.toString(response.getEntity());
        log.info("getEmailHistory() :: " + statusCode);
        log.info("getEmailHistory() body :: " + sResponse);
        if (statusCode == 200) {
            ObjectMapper mapper = new ObjectMapper();
            activities = mapper.readValue(sResponse,
                    new TypeReference<ArrayList<Activity>>() {});
        }
        return activities;
    }



    @Override
    public EmailContent getEmailContent(String id) throws Exception {
        EmailContent emailContent = new EmailContent();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress + "/journey/content?id="+id);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        String sResponse = EntityUtils.toString(response.getEntity());
        log.info("getEmailContent() :: " + statusCode);
        log.info("getEmailContent() body :: " + sResponse);
        if (statusCode == 200) {
            ObjectMapper mapper = new ObjectMapper();
            emailContent = mapper.readValue(sResponse,EmailContent.class);
        }
        return emailContent;
    }

    @Override
    public List<Activity> getConversationByEmail(String agentId, String agentName, String email) throws Exception {
        List<Activity> allActivities = new ArrayList<>();
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        try {
            httpclient.start();
            final CountDownLatch latchSocmed = new CountDownLatch(1);
            final  HttpGet httpGetSocmed = new HttpGet(hubAddress + "/journey/social-media?email="+email);
            httpGetSocmed.setHeader("Content-type", "application/json");
            httpclient.execute(httpGetSocmed, new FutureCallback<HttpResponse>() {
                public void completed(final HttpResponse response) {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + "-> httpGetSocmed" + response.getStatusLine());
                    int statusCode = response.getStatusLine().getStatusCode();
                    String sResponse = "";
                    try {
                        sResponse = EntityUtils.toString(response.getEntity());
                        if (statusCode == 200) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Activity> activities = mapper.readValue(sResponse,
                                    new TypeReference<ArrayList<Activity>>() {});
                            allActivities.addAll(activities);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                public void failed(final Exception ex) {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + "-> httpGetSocmed" + ex);
                }

                public void cancelled() {
                    latchSocmed.countDown();
                    System.out.println(httpGetSocmed.getRequestLine() + " httpGetSocmed cancelled");
                }

            });
            latchSocmed.await();
            final CountDownLatch latchEmail = new CountDownLatch(1);
            final  HttpGet httpGetEmail = new HttpGet(hubAddress + "/journey/email?email="+email);
            httpGetEmail.setHeader("Content-type", "application/json");
            httpclient.execute(httpGetEmail, new FutureCallback<HttpResponse>() {
                public void completed(final HttpResponse response) {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + "-> httpGetEmail" + response.getStatusLine());
                    int statusCode = response.getStatusLine().getStatusCode();
                    String sResponse = "";
                    try {
                        sResponse = EntityUtils.toString(response.getEntity());
                        if (statusCode == 200) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Activity> activities = mapper.readValue(sResponse,
                                    new TypeReference<ArrayList<Activity>>() {});
                            allActivities.addAll(activities);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                public void failed(final Exception ex) {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + "-> httpGetEmail" + ex);
                }

                public void cancelled() {
                    latchEmail.countDown();
                    System.out.println(httpGetEmail.getRequestLine() + " httpGetEmail cancelled");
                }

            });
            latchEmail.await();


        } finally {
            httpclient.close();
        }
        return allActivities;
    }

    @Override
    public Customer addMainCustomer(String phoneNumber, String agentId, String agentName,  String callKey, String callKeyDay) {
        Customer customer = new Customer();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        customer.setPhoneNumber(phoneNumber);
        customer.setCreatedIdBy(agentId);
        customer.setCreatedNameBy(agentName);
        customer.setCreatedAt(timestamp);
        customer.setModifiedIdBy(agentId);
        customer.setModifiedNameBy(agentName);
        customer.setModifiedAt(timestamp);
        customer.setCallKey(callKey);
        customer.setCallKeyDay(callKeyDay);
        customerRepository.save(customer);
        return customer;
    }

    @Override
    public Customer addPhoneMapping(Customer customer, String phoneNumber, String agentId, String agentName, String callKey, String callKeyDay) {
        PhoneMapping phoneMapping = new PhoneMapping();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        phoneMapping.setCustomer(customer);
        phoneMapping.setPhoneNumber(phoneNumber);
        phoneMapping.setCallKey(callKey);
        phoneMapping.setCallKeyDay(callKeyDay);
        phoneMapping.setCreatedIdBy(agentId);
        phoneMapping.setCreatedNameBy(agentName);
        phoneMapping.setCreatedAt(timestamp);
        phoneMapping.setModifiedIdBy(agentId);
        phoneMapping.setModifiedNameBy(agentName);
        phoneMapping.setModifiedAt(timestamp);
        phoneMappingRepository.save(phoneMapping);
        return customer;
    }

    @Override
    public Customer replaceCustomer(Customer customer, String newPhoneNumber, String agentId, String agentName, String callKey, String callKeyDay) {
        Customer currentCustomer = customerRepository.findByPhoneNumber(newPhoneNumber);
        currentCustomer.setPhoneNumber(newPhoneNumber);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        List<PhoneMapping> phoneMappingList = phoneMappingRepository.findAllByCustomer(customer);
        for (PhoneMapping phoneMapping : phoneMappingList){
        //    log.info("replaceCustomer() :: phone mapping "+data.getCustomer().getPhoneNumber());
       //     PhoneMapping phoneMapping = phoneMappingRepository.getOne(data.getId());
            phoneMapping.setCustomer(currentCustomer);
            phoneMapping.setModifiedIdBy(agentId);
            phoneMapping.setModifiedNameBy(agentName);
            phoneMapping.setModifiedAt(timestamp);
            phoneMappingRepository.save(phoneMapping);
        }
        PhoneMapping newPhoneMapping = new PhoneMapping();
        newPhoneMapping.setCustomer(currentCustomer);
        newPhoneMapping.setPhoneNumber(customer.getPhoneNumber());
        newPhoneMapping.setCreatedIdBy(agentId);
        newPhoneMapping.setCreatedNameBy(agentName);
        newPhoneMapping.setCreatedAt(timestamp);
        newPhoneMapping.setStartedAt(customer.getCreatedAt());
        newPhoneMapping.setModifiedIdBy(agentId);
        newPhoneMapping.setModifiedNameBy(agentName);
        newPhoneMapping.setModifiedAt(timestamp);
        newPhoneMapping.setLastCallKey(callKey);
        newPhoneMapping.setLastCallKeyDay(callKeyDay);
        newPhoneMapping.setCallKey(customer.getCallKey());
        newPhoneMapping.setCallKeyDay(customer.getCallKeyDay());
        phoneMappingRepository.save(newPhoneMapping);
        customerRepository.delete(customer);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return currentCustomer;
    }

    @Override
    public void removePhones(Long[] ids) throws Exception {
        for(Long id : ids){
            log.info("removePhones() :: id "+id);
            PhoneMapping phoneMapping = phoneMappingRepository.getOne(id);
            if(phoneMapping != null){
                phoneMappingRepository.delete(phoneMapping);
            }
        }
    }


}
