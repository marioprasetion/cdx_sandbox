package com.msi.cdx.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.msi.cdx.model.pcce.ActivityHub;
import com.msi.cdx.model.internal.EmailContent;
import com.msi.cdx.util.SslUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@Transactional
@Slf4j
public class JourneyServiceImpl implements JourneyService {

    private final int PAGE_SIZE = 75;
    private final int SOCMED_PAGE_SIZE = 20;
    @Value("${hub2.address}")
    private String hubAddress;

    @Value("${socmed-controller.address}")
    private String socmedAddress;

    @Value("${socmed-controller.username}")
    private String socmedUsername;

    @Value("${socmed-controller.password}")
    private String socmedPassword;


    @Override
    public List<ActivityHub> getEmailHistory(String email) throws Exception {
        List<ActivityHub> activities = new ArrayList<>();
        ArrayList<Long> activityIdList = getActivityIdList(email);
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        try{
            httpclient.start();
            for (Long activityId : activityIdList) {
                int pagenum = 1;
                final CountDownLatch latchEmail = new CountDownLatch(1);
                final  HttpGet httpGetEmail = new HttpGet(hubAddress + "/interaction/activity/"+activityId);
                httpGetEmail.setHeader("Content-type", "application/json");
                httpclient.execute(httpGetEmail, new FutureCallback<HttpResponse>() {
                    public void completed(final HttpResponse response) {
                        latchEmail.countDown();
                        System.out.println(httpGetEmail.getRequestLine() + "-> httpGetEmail" + response.getStatusLine());
                        int statusCode = response.getStatusLine().getStatusCode();
                        String sResponse = "";
                        try {

                            log.info("getEmailHistory() :: " + statusCode);
                            if (statusCode == 200) {
                                sResponse = EntityUtils.toString(response.getEntity());
                                ObjectMapper mapper = new ObjectMapper();
                                log.info("getEmailHistory() body :: " + sResponse);
                                JSONObject jsonObject = new JSONObject(sResponse);
                                JSONArray jArrCases = jsonObject.getJSONArray("activity");
                                ActivityHub activity = new ActivityHub();
                                if(jArrCases.getJSONObject(0).getJSONObject("status").has("assigned")){
                                    if(jArrCases.getJSONObject(0).getJSONObject("status").getJSONObject("assigned").has("user")){
                                        JSONObject joUser = jArrCases.getJSONObject(0)
                                                .getJSONObject("status")
                                                .getJSONObject("assigned")
                                                .getJSONObject("user");
                                        activity.setAgentId(joUser.getInt("id")+"");
                                        activity.setAgentName(joUser.getString("name"));
                                    }
                                }
                                activity.setContent(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                                        .getJSONObject("contents").getJSONArray("content").getJSONObject(0).getString("value"));
                                activity.setSource(jArrCases.getJSONObject(0).getJSONObject("type").getString("value"));
                                activity.setType(jArrCases.getJSONObject(0).getJSONObject("mode").getString("value"));
                                activity.setName(email);
                                activity.setDatetime(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                                        .getJSONObject("date").getString("date"));
                                activity.setSubject(jArrCases.getJSONObject(0).getString("subject"));
                                activity.setConversationId(jArrCases.getJSONObject(0).getJSONObject("case").getInt("id")+"");
                                activities.add(activity);
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void failed(final Exception ex) {
                        latchEmail.countDown();
                        System.out.println(httpGetEmail.getRequestLine() + "-> getEmailHistory()" + ex);
                    }

                    public void cancelled() {
                        latchEmail.countDown();
                        System.out.println(httpGetEmail.getRequestLine() + " getEmailHistory() cancelled");
                    }

                });
                latchEmail.await();
            }
        }finally{
            httpclient.close();
        }
        return activities;
    }




    @Override
    public List<ActivityHub> getSocialHistory(String email) throws Exception {
        List<ActivityHub> activities = new ArrayList<>();
        ArrayList<Long> caseIdList = getCaseIdChatList(email);
  //      ArrayList<Long> caseIdList = new ArrayList<>();
  //      caseIdList.add(1369L);
        for (Long caseId : caseIdList){
            boolean hasMoreData = true;
            String nextData = "";
            while(hasMoreData) {
                CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                        .setSSLContext(SslUtils.getSslContext()).build();
                HttpGet httpGet = new HttpGet(socmedAddress + "/conversation/" + caseId + "/messages?sender_type=customer");
                //  HttpGet httpGet = new HttpGet(socmedAddress + "/test/socmed.php");
                httpGet.setHeader("Content-type", "application/json");
                httpGet.setHeader("x-token", getSocmedToken());
                if(nextData.length() > 0){
                    httpGet.setHeader("next", nextData);
                }
                CloseableHttpResponse response = httpClient.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                log.info("getSocialHistory() :: " + statusCode);
                if (statusCode == 200) {
                    String sResponse = EntityUtils.toString(response.getEntity());
                    log.info("getSocialHistory() body :: " + sResponse);
                    JSONObject jsonObject = new JSONObject(sResponse);
                    JSONArray jArrMessages = jsonObject.getJSONArray("items");
                    for (int i = 0; i < jArrMessages.length(); i++) {
                        ActivityHub activity = new ActivityHub();
                        JSONObject objCase = jArrMessages.getJSONObject(i);
                        String originalId = objCase.getString("originalId");
                        String content = objCase.getString("content");
                        String createdAt = objCase.getString("createdAt");
                        String type = objCase.getJSONObject("sender").getString("type");
                        String name = objCase.getJSONObject("sender").getString("name");
                        String channel = objCase.getJSONObject("sender").has("channel") ? objCase.getJSONObject("sender").getString("channel") : "";
                        //     activity.setConversationId(caseId+"");
                        activity.setConversationId(""+caseId);
                        activity.setName(name);
                        if (type.equals("customer")) {
                            activity.setType("inbound");
                        } else {
                            activity.setType("outbound");
                        }
                        activity.setSource(channel);
                        activity.setContent(content);
                        activity.setDatetime(createdAt);
                        activities.add(activity);
                    }
                    if( jArrMessages.length() < SOCMED_PAGE_SIZE){
                        hasMoreData = false;
                    }
                    nextData = jsonObject.getJSONObject("paging").getJSONObject("cursor").getString("next");
                }else{
                    hasMoreData = false;
                }

            }

        }
        return activities;
    }

    @Override
    public List<ActivityHub> getEmailDetails(String conversationId) throws Exception {
        List<ActivityHub> activities = new ArrayList<>();
        ArrayList<Long> activityIdList = getActivityIdListByCaseId(conversationId);
        for (Long activityId : activityIdList) {
            int pagenum = 1;
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            HttpGet httpGet = new HttpGet(hubAddress + "/interaction/activity/"+activityId);
            httpGet.setHeader("Content-type", "application/json");
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("getEmailHistory() :: " + statusCode);
            if (statusCode == 200) {
                String sResponse = EntityUtils.toString(response.getEntity());
                log.info("getEmailHistory() body :: " + sResponse);
                JSONObject jsonObject = new JSONObject(sResponse);
                JSONArray jArrCases = jsonObject.getJSONArray("activity");
                ActivityHub activity = new ActivityHub();
                if(jArrCases.getJSONObject(0).getJSONObject("status").has("assigned")){
                    if(jArrCases.getJSONObject(0).getJSONObject("status").getJSONObject("assigned").has("user")){
                        JSONObject joUser = jArrCases.getJSONObject(0)
                                .getJSONObject("status")
                                .getJSONObject("assigned")
                                .getJSONObject("user");
                        activity.setAgentId(joUser.getInt("id")+"");
                        activity.setAgentName(joUser.getString("name"));
                    }
                }
                activity.setContent(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                        .getJSONObject("contents").getJSONArray("content").getJSONObject(0).getString("value"));
                activity.setSource(jArrCases.getJSONObject(0).getJSONObject("type").getString("value"));
                activity.setType(jArrCases.getJSONObject(0).getJSONObject("mode").getString("value"));
                activity.setName(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                        .getJSONObject("emailAddresses").getString("from"));
                activity.setDatetime(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                        .getJSONObject("date").getString("date"));
                activity.setSubject(jArrCases.getJSONObject(0).getString("subject"));
                activity.setConversationId(jArrCases.getJSONObject(0).getJSONObject("case").getInt("id")+"");
                activities.add(activity);
            }
        }
        return activities;
    }

    @Override
    public List<ActivityHub> getSocialDetails(String conversationId) throws Exception {
        List<ActivityHub> activities = new ArrayList<>();
        boolean hasMoreData = true;
        String nextData = "";
        while(hasMoreData) {
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            HttpGet httpGet = new HttpGet(socmedAddress + "/conversation/" + conversationId + "/messages");
            //  HttpGet httpGet = new HttpGet(socmedAddress + "/test/socmed.php");
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("x-token", getSocmedToken());
            if(nextData.length() > 0){
                httpGet.setHeader("next", nextData);
            }
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("getSocialHistory() :: " + statusCode);
            if (statusCode == 200) {
                String sResponse = EntityUtils.toString(response.getEntity());
                log.info("getSocialHistory() body :: " + sResponse);
                JSONObject jsonObject = new JSONObject(sResponse);
                JSONArray jArrMessages = jsonObject.getJSONArray("items");
                for (int i = 0; i < jArrMessages.length(); i++) {
                    ActivityHub activity = new ActivityHub();
                    JSONObject objCase = jArrMessages.getJSONObject(i);
                    String originalId = objCase.getString("originalId");
                    String content = objCase.getString("content");
                    String createdAt = objCase.getString("createdAt");
                    String type = objCase.getJSONObject("sender").getString("type");
                    String name = objCase.getJSONObject("sender").getString("name");
                    String channel = objCase.getJSONObject("sender").has("channel") ? objCase.getJSONObject("sender").getString("channel") : "";
                    //     activity.setConversationId(caseId+"");
                    activity.setConversationId(conversationId);
                    activity.setName(name);
                    if (type.equals("customer")) {
                        activity.setType("inbound");
                    } else {
                        activity.setType("outbound");
                    }
                    activity.setSource(channel);
                    activity.setContent(content);
                    activity.setDatetime(createdAt);
                    activities.add(activity);
                }
                if( jArrMessages.length() < SOCMED_PAGE_SIZE){
                    hasMoreData = false;
                }
                nextData = jsonObject.getJSONObject("paging").getJSONObject("cursor").getString("next");
            }else{
                hasMoreData = false;
            }

        }
        return activities;
    }

    @Override
    public EmailContent getEmailContent(String id) throws Exception {
        EmailContent emailContent = new EmailContent();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress + "/interaction/activity/"+id);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        log.info("getEmailContent() :: " + statusCode);
        if (statusCode == 200) {
            String sResponse = EntityUtils.toString(response.getEntity());
            log.info("getEmailContent() body :: " + sResponse);
            JSONObject jsonObject = new JSONObject(sResponse);
            JSONArray jArrCases = jsonObject.getJSONArray("activity");
            emailContent.setContentId(id);
            emailContent.setContent(jArrCases.getJSONObject(0).getJSONObject("payload").getJSONObject("email")
                    .getJSONObject("contents").getJSONArray("content").getJSONObject(0).getString("value"));
        }
        return emailContent;
    }

    private String getSocmedToken() throws Exception{
        String token = "";
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpPost httpPost = new HttpPost(socmedAddress + "/user/token/create");
        List <NameValuePair> nvps = new ArrayList <NameValuePair>();
        nvps.add(new BasicNameValuePair("username", socmedUsername));
        nvps.add(new BasicNameValuePair("password", socmedPassword));
        httpPost.setEntity(new UrlEncodedFormEntity(nvps));
        CloseableHttpResponse response = httpClient.execute(httpPost);
        int statusCode = response.getStatusLine().getStatusCode();
        log.info("getSocmedToken() :: " + statusCode);
        if (statusCode == 200) {
            String sResponse = EntityUtils.toString(response.getEntity());
            log.info("getSocmedToken() body :: " + sResponse);
            JSONObject jsonObject = new JSONObject(sResponse);
            token = jsonObject.getJSONObject("value").getJSONObject("token").getString("hash");
            return token;
        }

        return token;
    }

    private ArrayList<Long> getActivityIdListByCaseId(String caseId) throws Exception {
        ArrayList<Long> activityIdList = new ArrayList<>();
        int pagenum = 1;
        boolean hasMoreData = true;
        while(hasMoreData){
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            HttpGet httpGet = new HttpGet(hubAddress+"/interaction/activity?case="+caseId+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
            httpGet.setHeader("Content-type", "application/json");
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("getActivityIdListByCaseId() :: "+statusCode);
            if (statusCode == 200){
                String sResponse = EntityUtils.toString(response.getEntity());
                log.info("getActivityIdListByCaseId() body :: "+sResponse);
                JSONObject jsonObject = new JSONObject(sResponse);
                JSONArray jArrCases = jsonObject.getJSONArray("activity");
                for (int i = 0 ; i < jArrCases.length(); i++)
                {
                    JSONObject objCase = jArrCases.getJSONObject(i);
                    String type = objCase.getJSONObject("type").getString("value");
                    Long id = objCase.getLong("id");
                    if(type.equals("email")){
                        log.info("id activity : "+id);
                        activityIdList.add(id);
                    }
                }
                JSONObject paginationInfo = jsonObject.getJSONObject("paginationInfo");
                int count = paginationInfo.getInt("count");
                if(count < PAGE_SIZE){
                    hasMoreData = false;
                }
                pagenum ++;
            }else{
                break;
            }
        }


        return activityIdList;

    }
    public List<ActivityHub> getEmailHistoryWoContent(String email) throws Exception {
        List<ActivityHub> activities = new ArrayList<>();
        ArrayList<Long> caseList = getCaseIdEmailList(email);
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        try{
            httpclient.start();
            for (Long caseId : caseList) {
                int pagenum = 1;
                final CountDownLatch latchEmail = new CountDownLatch(1);
                final  HttpGet httpGet = new HttpGet(hubAddress+"/interaction/activity?case="+caseId+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
                httpGet.setHeader("Content-type", "application/json");
                httpclient.execute(httpGet, new FutureCallback<HttpResponse>() {
                    public void completed(final HttpResponse response) {
                        latchEmail.countDown();
                        System.out.println(httpGet.getRequestLine() + "-> httpGetEmail getEmailHistoryWoContent" + response.getStatusLine());
                        int statusCode = response.getStatusLine().getStatusCode();
                        String sResponse = "";
                        try {
                            log.info("getEmailHistoryWoContent() :: " + statusCode);
                            if (statusCode == 200) {
                                sResponse = EntityUtils.toString(response.getEntity());
                                JSONObject jsonObject = new JSONObject(sResponse);
                                JSONArray jArrCases = jsonObject.getJSONArray("activity");
                                log.info("getEmailHistoryWoContent() body :: " + sResponse);
                                for (int i = 0 ; i < jArrCases.length(); i++) {
                                    JSONObject objCase = jArrCases.getJSONObject(i);
                                    ExecutorService executor = Executors.newSingleThreadExecutor();
                                    executor.submit(() -> {
                                        try{
                                            ActivityHub activity = new ActivityHub();
                                            if(objCase.getJSONObject("status").has("assigned")){
                                                if(objCase.getJSONObject("status").getJSONObject("assigned").has("user")){
                                                    JSONObject joUser = objCase
                                                            .getJSONObject("status")
                                                            .getJSONObject("assigned")
                                                            .getJSONObject("user");
                                                    activity.setAgentId(joUser.getInt("id")+"");
                                                    activity.setAgentName(joUser.getString("name"));
                                                }
                                            }
                                            activity.setContent(objCase.getString("id"));
                                            activity.setSource(objCase.getJSONObject("type").getString("value"));
                                            activity.setType(objCase.getJSONObject("mode").getString("value"));
                                            activity.setName(objCase.getJSONObject("payload").getJSONObject("email")
                                                    .getJSONObject("emailAddresses").getString("from"));
                                            activity.setDatetime(objCase.getJSONObject("payload").getJSONObject("email")
                                                    .getJSONObject("date").getString("date"));
                                            activity.setSubject(jArrCases.getJSONObject(0).getString("subject"));
                                            activity.setConversationId(objCase.getJSONObject("case").getInt("id")+"");
                                            activities.add(activity);
                                        }catch (Exception e){
                                            log.error("getEmailHistoryWoContent() executor ", e);
                                        }

                                    });

                                }

                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    public void failed(final Exception ex) {
                        latchEmail.countDown();
                        System.out.println(httpGet.getRequestLine() + "-> getEmailHistoryWoContent()" + ex);
                    }

                    public void cancelled() {
                        latchEmail.countDown();
                        System.out.println(httpGet.getRequestLine() + " getEmailHistoryWoContent() cancelled");
                    }

                });
                latchEmail.await();
            }
        }finally{
            httpclient.close();
        }
        return activities;
    }
    private ArrayList<Long> getActivityIdList(String email) throws Exception {
        ArrayList<Long> caseList = getCaseIdEmailList(email);
        ArrayList<Long> activityIdList = new ArrayList<>();
        for (Long caseId : caseList){
            int pagenum = 1;
            boolean hasMoreData = true;
            while(hasMoreData){
                CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                        .setSSLContext(SslUtils.getSslContext()).build();
                HttpGet httpGet = new HttpGet(hubAddress+"/interaction/activity?case="+caseId+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
                httpGet.setHeader("Content-type", "application/json");
                CloseableHttpResponse response = httpClient.execute(httpGet);
                int statusCode = response.getStatusLine().getStatusCode();
                log.info("getActivityIdList() :: "+statusCode);
                if (statusCode == 200){
                    String sResponse = EntityUtils.toString(response.getEntity());
                    log.info("getActivityIdList() body :: "+sResponse);
                    JSONObject jsonObject = new JSONObject(sResponse);
                    JSONArray jArrCases = jsonObject.getJSONArray("activity");
                    for (int i = 0 ; i < jArrCases.length(); i++)
                    {
                        JSONObject objCase = jArrCases.getJSONObject(i);
                        String type = objCase.getJSONObject("type").getString("value");
                        String mode = objCase.getJSONObject("mode").getString("value");
                        Long id = objCase.getLong("id");
                        if(type.equals("email")){
                            log.info("id activity : "+id);
                            if(mode.equals("inbound")){
                                activityIdList.add(id);
                            }

                        }
                    }
                    JSONObject paginationInfo = jsonObject.getJSONObject("paginationInfo");
                    int count = paginationInfo.getInt("count");
                    if(count < PAGE_SIZE){
                        hasMoreData = false;
                    }
                    pagenum ++;
                }else{
                    break;
                }
            }
        }
        return activityIdList;
    }

    private ArrayList<Long> getCaseIdChatList(String email) throws Exception {
        int pagenum = 1;
        ArrayList<Long> caseList = new ArrayList<>();
        boolean hasMoreData = true;
        while(hasMoreData){
            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setSSLContext(SslUtils.getSslContext()).build();
            HttpGet httpGet = new HttpGet(hubAddress+"/interaction/case?email="+email+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
            httpGet.setHeader("Content-type", "application/json");
            CloseableHttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("getCaseIdChatList() :: "+statusCode);
            if (statusCode == 200){
                String sResponse = EntityUtils.toString(response.getEntity());
                log.info("getCaseIdChatList() body :: "+sResponse);
                JSONObject jsonObject = new JSONObject(sResponse);
                JSONArray jArrCases = jsonObject.getJSONArray("case");
                for (int i = 0 ; i < jArrCases.length(); i++)
                {
                    JSONObject objCase = jArrCases.getJSONObject(i);
                    String originatingChannel = objCase.
                            getJSONObject("originatingChannel").getString("value");
                    Long id = objCase.getLong("id");
                    log.info("getCaseIdChatList() id case : "+id);
                    log.info("getCaseIdChatList() originatingChannel : "+originatingChannel);
                    if(!originatingChannel.equals("Email")){
                        caseList.add(id);
                    }
                }
                JSONObject paginationInfo = jsonObject.getJSONObject("paginationInfo");
                int count = paginationInfo.getInt("count");
                if(count < PAGE_SIZE){
                    hasMoreData = false;
                }
                pagenum ++;
            }else{
                break;
            }
        }
        log.info("getCaseIdChatList() end : ");
        return caseList;
    }

    private ArrayList<Long> getCaseIdEmailList(String email) throws Exception {
        int pagenum = 1;
        ArrayList<Long> caseList = new ArrayList<>();
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSSLContext(SslUtils.getSslContext()).build();
        HttpGet httpGet = new HttpGet(hubAddress+"/interaction/case?email="+email+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
        httpGet.setHeader("Content-type", "application/json");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        int statusCode = response.getStatusLine().getStatusCode();
        log.info("getCaseIdList() :: "+statusCode);
        if (statusCode == 200){
            String sResponse = EntityUtils.toString(response.getEntity());
            log.info("getCaseIdList() body :: "+sResponse);
            JSONObject jsonObject = new JSONObject(sResponse);
            JSONArray jArrCases = jsonObject.getJSONArray("case");
            for (int i = 0 ; i < jArrCases.length(); i++)
            {
                JSONObject objCase = jArrCases.getJSONObject(i);
                String originatingChannel = objCase.
                        getJSONObject("originatingChannel").getString("value");
                Long id = objCase.getLong("id");
                log.info("id case : "+id);
                if(originatingChannel.equals("Email")){
                    caseList.add(id);
                }
            }
            JSONObject paginationInfo = jsonObject.getJSONObject("paginationInfo");
            int count = paginationInfo.getInt("count");
        }
        return caseList;
    }

// full size
//    private ArrayList<Long> getCaseIdEmailList(String email) throws Exception {
//        int pagenum = 1;
//        ArrayList<Long> caseList = new ArrayList<>();
//        boolean hasMoreData = true;
//        while(hasMoreData){
//            CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
//                    .setSSLContext(SslUtils.getSslContext()).build();
//            HttpGet httpGet = new HttpGet(hubAddress+"/interaction/case?email="+email+"&pageNum="+pagenum+"&pageSize="+PAGE_SIZE);
//            httpGet.setHeader("Content-type", "application/json");
//            CloseableHttpResponse response = httpClient.execute(httpGet);
//            int statusCode = response.getStatusLine().getStatusCode();
//            log.info("getCaseIdList() :: "+statusCode);
//            if (statusCode == 200){
//                String sResponse = EntityUtils.toString(response.getEntity());
//                log.info("getCaseIdList() body :: "+sResponse);
//                JSONObject jsonObject = new JSONObject(sResponse);
//                JSONArray jArrCases = jsonObject.getJSONArray("case");
//                for (int i = 0 ; i < jArrCases.length(); i++)
//                {
//                    JSONObject objCase = jArrCases.getJSONObject(i);
//                    String originatingChannel = objCase.
//                            getJSONObject("originatingChannel").getString("value");
//                    Long id = objCase.getLong("id");
//                    log.info("id case : "+id);
//                    if(originatingChannel.equals("Email")){
//                        caseList.add(id);
//                    }
//                }
//                JSONObject paginationInfo = jsonObject.getJSONObject("paginationInfo");
//                int count = paginationInfo.getInt("count");
//                if(count < PAGE_SIZE){
//                    hasMoreData = false;
//                }
//                //need comment
//                if(caseList.size() > 100){
//                    hasMoreData = false;
//                }
//                pagenum ++;
//            }else{
//                break;
//            }
//        }
//        return caseList;
//    }
}
