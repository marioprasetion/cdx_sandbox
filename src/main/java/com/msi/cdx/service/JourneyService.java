package com.msi.cdx.service;

import com.msi.cdx.model.pcce.ActivityHub;
import com.msi.cdx.model.internal.EmailContent;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface JourneyService {
    List<ActivityHub> getEmailHistory(String email) throws Exception;
    List<ActivityHub> getEmailHistoryWoContent(String email) throws Exception;
    List<ActivityHub> getSocialHistory(String email) throws Exception;
    List<ActivityHub> getEmailDetails(String conversationId) throws Exception;
    List<ActivityHub> getSocialDetails(String conversationId) throws Exception;
    EmailContent getEmailContent(String id) throws Exception;
}
