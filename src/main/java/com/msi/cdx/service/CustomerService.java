package com.msi.cdx.service;

import com.msi.cdx.model.internal.Activity;
import com.msi.cdx.model.internal.Customer;
import com.msi.cdx.model.internal.EmailContent;
import com.msi.cdx.model.internal.Journey;

import java.util.List;

public interface CustomerService {
    Customer save(Customer customer) throws Exception;
    Journey getActivities(String agentId, String agentName, Customer customer, String email) throws Exception;
    List<Activity> getActivitiesByCustomer(String agentId, String agentName, Customer customer) throws Exception;
    List<Activity> getConversation(String agentId, String agentName, String conversationId, String source,
                                   String phoneNumber, String searchPhoneNumber) throws Exception;
    Customer getCustomerByPhone(String phoneNumber) throws Exception;
    EmailContent getEmailContent(String id) throws Exception;
    List<Activity> getConversationByEmail(String agentId, String agentName, String email) throws Exception;

    Customer addMainCustomer(String phoneNumber, String agentId, String agentName,  String callKey, String callKeyDay);

    Customer addPhoneMapping(Customer customer, String phoneNumber, String agentId, String agentName, String callKey, String callKeyDay);

    Customer replaceCustomer(Customer customer, String newPhoneNumber, String agentId, String agentName, String callKey, String callKeyDay);

    void removePhones(Long[] ids)throws Exception;

}
