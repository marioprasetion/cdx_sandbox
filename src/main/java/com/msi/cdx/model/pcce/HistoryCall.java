package com.msi.cdx.model.pcce;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@SqlResultSetMapping(
        name="myMapping",
        classes={
                @ConstructorResult(
                        targetClass=HistoryCall.class,
                        columns={
                                @ColumnResult(name="firstName" , type = String.class),
                                @ColumnResult(name="lastName", type = String.class),
                                @ColumnResult(name="loginName", type = String.class),
                                @ColumnResult(name="queue", type = String.class),
                                @ColumnResult(name="dateTime", type = String.class),
                                @ColumnResult(name="date", type = String.class),
                                @ColumnResult(name="agent", type = String.class),
                                @ColumnResult(name="skillGroup", type = String.class),
                                @ColumnResult(name="callDisposition", type = String.class),
                                @ColumnResult(name="digitsDialed", type = String.class),
                                @ColumnResult(name="dnis", type = String.class),
                                @ColumnResult(name="ani", type = String.class),
                                @ColumnResult(name="talkTime", type = String.class),
                                @ColumnResult(name="ringTime", type = String.class),
                                @ColumnResult(name="timeToAband", type = String.class),
                                @ColumnResult(name="duration", type = String.class),
                                @ColumnResult(name="holdTime", type = String.class),
                                @ColumnResult(name="localQTime", type = String.class),
                                @ColumnResult(name="workTime", type = String.class),
                                @ColumnResult(name="wrapupData", type = String.class),
                                @ColumnResult(name="variable1", type = String.class),
                                @ColumnResult(name="variable2", type = String.class),
                                @ColumnResult(name="variable3", type = String.class),
                                @ColumnResult(name="variable4", type = String.class),
                                @ColumnResult(name="variable5", type = String.class),
                                @ColumnResult(name="variable6", type = String.class),
                                @ColumnResult(name="variable7", type = String.class),
                                @ColumnResult(name="variable8", type = String.class),
                                @ColumnResult(name="variable9", type = String.class),
                                @ColumnResult(name="variable10", type = String.class),
                                @ColumnResult(name="callDispositionFlag", type = String.class),
                                @ColumnResult(name="skillTargetID", type = String.class),
                        }
                )
        }
)
@NamedNativeQuery(name="HistoryCall.getHistoryCall",
        resultSetMapping="myMapping",
        query="SELECT \n" +
                "firstName = Person.FirstName, \n" +
                "lastName = Person.LastName, \n" +
                "loginName = Person.LoginName,\n" +
                "queue = Variable3,\n" +
                "DateTime as datetime,  DateTime as date,\n" +
                "Agent.EnterpriseName as agent,\n" +
                "Skill_Group.EnterpriseName as skillGroup,\n" +
                "CASE TCD.CallDisposition \n" +
                "WHEN 1 THEN 'Abandoned in Network'\n" +
                "WHEN 2 THEN 'Abandoned in Local Queue'\n" +
                "WHEN 3 THEN 'Abandoned Ring'\n" +
                "WHEN 4 THEN 'Abandoned Delay'\n" +
                "WHEN 5 THEN 'Abandoned Interflow'\n" +
                "WHEN 6 THEN 'Abandoned Agent Terminal'\n" +
                "WHEN 7 THEN 'Short'\n" +
                "WHEN 8 THEN 'Busy'\n" +
                "WHEN 9 THEN 'Forced Busy'\n" +
                "WHEN 10 THEN 'Disconnect / Drop No Answer'\n" +
                "WHEN 11 THEN 'Disconnect / Drop Busy'\n" +
                "WHEN 12 THEN 'Disconnect / Drop Reorder'\n" +
                "WHEN 13 THEN 'Disconnect / Drop Handled Primary Route'\n" +
                "WHEN 14 THEN 'Disconnect / Drop Handled Other'\n" +
                "WHEN 15 THEN 'Redirected'\n" +
                "WHEN 16 THEN 'Cut Through'\n" +
                "WHEN 17 THEN 'Intraflow'\n" +
                "WHEN 18 THEN 'Interflow'\n" +
                "WHEN 19 THEN 'Ring No Answer'\n" +
                "WHEN 20 THEN 'Intercept Reorder'\n" +
                "WHEN 21 THEN 'Intercept Denial'\n" +
                "WHEN 22 THEN 'Time Out'\n" +
                "WHEN 23 THEN 'Voice Energy'\n" +
                "WHEN 24 THEN 'Non-Classified Energy Detected'\n" +
                "WHEN 25 THEN 'No Cut Through'\n" +
                "WHEN 26 THEN 'U-Abort'\n" +
                "WHEN 27 THEN 'Failed Software'\n" +
                "WHEN 28 THEN 'Blind Transfer'\n" +
                "WHEN 29 THEN 'Announced Transfer'\n" +
                "WHEN 30 THEN 'Conferenced'\n" +
                "WHEN 31 THEN 'Duplicate Transfer'\n" +
                "WHEN 32 THEN 'Unmonitored Device'\n" +
                "WHEN 33 THEN 'Answering Machine'\n" +
                "WHEN 34 THEN 'Network Blind Transfer'\n" +
                "WHEN 35 THEN 'Task Abandoned in Router'\n" +
                "WHEN 36 THEN 'Task Abandoned Before Offered'\n" +
                "WHEN 37 THEN 'Task Abandoned While Offered'\n" +
                "WHEN 38 THEN 'Normal Task End'\n" +
                "WHEN 39 THEN 'Cant Obtain Task ID'\n" +
                "WHEN 40 THEN 'Agent Logged Out During Task'\n" +
                "WHEN 41 THEN 'Maximum Task Lifetime Exceeded'\n" +
                "WHEN 42 THEN 'Application Path Went Down'\n" +
                "WHEN 51 THEN 'Task Ended During Application Init'\n" +
                "WHEN 53 THEN 'Partial Call'\n" +
                "ELSE 'Unknown'  \n" +
                "END as callDisposition,\n" +
                "TCD.DigitsDialed as digitsDialed,\n" +
                "TCD.DNIS as dnis,\n" +
                "TCD.ANI as ani,\n" +
                "TCD.TalkTime as talkTime,\n" +
                "TCD.RingTime as ringTime,\n" +
                "TCD.TimeToAband as timeToAband,\n" +
                "TCD.Duration as duration,\n" +
                "TCD.HoldTime as holdTime,\n" +
                "TCD.LocalQTime as localQTime,\n" +
                "TCD.WorkTime as workTime,\n" +
                "TCD.WrapupData as wrapUpData,\n" +
                "TCD.Variable1 as variable1,\n" +
                "TCD.Variable2 as variable2,\n" +
                "TCD.Variable3 as variable3,\n" +
                "TCD.Variable4 as variable4,\n" +
                "TCD.Variable5 as variable5,\n" +
                "TCD.Variable6 as variable6,\n" +
                "TCD.Variable7 as variable7,\n" +
                "TCD.Variable8 as variable8,\n" +
                "TCD.Variable9 as variable9,\n" +
                "TCD.Variable10 as variable10,\n" +
                "CASE TCD.CallDispositionFlag\n" +
                "WHEN 1 THEN 'Handled'\n" +
                "WHEN 2 THEN 'Abandoned'\n" +
                "WHEN 3 THEN 'Short'\n" +
                "WHEN 4 THEN 'Disconnect'\n" +
                "WHEN 5 THEN 'Redirected'\n" +
                "WHEN 6 THEN 'Requery'\n" +
                "WHEN 7 THEN 'Incomplete'\n" +
                "ELSE 'Unknown'  \n" +
                "END as callDispositionFlag,\n" +
                "Agent.SkillTargetID as skillTargetID\n" +
                "FROM \n" +
                "Termination_Call_Detail TCD JOIN\n" +
                "Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\n" +
                "Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\n" +
                "LEFT JOIN Person ON Agent.PersonID = Person.PersonID\n" +
                "WHERE\n" +
                "Agent.EnterpriseName IS NOT NULL and TCD.CallDispositionFlag = 1 AND ANI= :ani \n" +
                "ORDER BY DateTime ASC")


public class HistoryCall {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id;
    private String firstName;
    private String lastName;
    private String loginName;
    private String queue;
    private String dateTime;
    private String date;
    private String agent;
    private String skillGroup;
    private String callDisposition;
    private String digitsDialed;
    private String dnis;
    private String ani;
    private String talkTime;
    private String ringTime;
    private String timeToAband;
    private String duration;
    private String holdTime;
    private String localQTime;
    private String workTime;
    private String wrapupData;
    private String variable1;
    private String variable2;
    private String variable3;
    private String variable4;
    private String variable5;
    private String variable6;
    private String variable7;
    private String variable8;
    private String variable9;
    private String variable10;
    private String callDispositionFlag;
    private String skillTargetID;

    public HistoryCall(String firstName, String lastName, String loginName, String queue, String dateTime, String date, String agent, String skillGroup, String callDisposition, String digitsDialed, String dnis, String ani, String talkTime, String ringTime, String timeToAband, String duration, String holdTime, String localQTime, String workTime, String wrapupData, String variable1, String variable2, String variable3, String variable4, String variable5, String variable6, String variable7, String variable8, String variable9, String variable10, String callDispositionFlag, String skillTargetID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.loginName = loginName;
        this.queue = queue;
        this.dateTime = dateTime;
        this.date = date;
        this.agent = agent;
        this.skillGroup = skillGroup;
        this.callDisposition = callDisposition;
        this.digitsDialed = digitsDialed;
        this.dnis = dnis;
        this.ani = ani;
        this.talkTime = talkTime;
        this.ringTime = ringTime;
        this.timeToAband = timeToAband;
        this.duration = duration;
        this.holdTime = holdTime;
        this.localQTime = localQTime;
        this.workTime = workTime;
        this.wrapupData = wrapupData;
        this.variable1 = variable1;
        this.variable2 = variable2;
        this.variable3 = variable3;
        this.variable4 = variable4;
        this.variable5 = variable5;
        this.variable6 = variable6;
        this.variable7 = variable7;
        this.variable8 = variable8;
        this.variable9 = variable9;
        this.variable10 = variable10;
        this.callDispositionFlag = callDispositionFlag;
        this.skillTargetID = skillTargetID;
    }

    //getters/setters/etc
}
