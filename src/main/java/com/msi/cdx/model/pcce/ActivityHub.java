package com.msi.cdx.model.pcce;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@SqlResultSetMapping(
        name="activityMapping",
        classes={
                @ConstructorResult(
                        targetClass=ActivityHub.class,
                        columns={
                                @ColumnResult(name="name" , type = String.class),
                                @ColumnResult(name="datetime", type = String.class),
                                @ColumnResult(name="content", type = String.class),
                                @ColumnResult(name="agentName", type = String.class),
                                @ColumnResult(name="agentId", type = String.class),
                                @ColumnResult(name="source", type = String.class),
                                @ColumnResult(name="duration", type = Long.class),
                                @ColumnResult(name="subject", type = String.class),
                                @ColumnResult(name="type", type = String.class),
                                @ColumnResult(name="conversationId", type = String.class),
                        }
                )
        }
)

@NamedNativeQueries({
        @NamedNativeQuery(name="ActivityHub.getHistoryCall",
                resultSetMapping="activityMapping",
                query="SELECT \n" +
                        "name = TCD.ANI,\n" +
                        "DateTime as datetime,\n" +
                        "TCD.WrapupData as content, \n" +
                        "CONCAT(Person.FirstName, ' ',Person.LastName) as agentName,\n" +
                        "agentId = Person.PersonID, \n" +
                        "source ='phone',\n" +
                        "duration = TCD.Duration,\n" +
                        "subject = '', \n" +
                        "type = 'inbound', \n" +
                        "conversationId = TCD.ANI\n" +
                        "FROM \n" +
                        "Termination_Call_Detail TCD JOIN\n" +
                        "Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\n" +
                        "Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\n" +
                        "LEFT JOIN Person ON Agent.PersonID = Person.PersonID\n" +
                        "WHERE\n" +
                        "Agent.EnterpriseName IS NOT NULL and TCD.CallDispositionFlag = 1 AND ANI= :ani \n" +
                        "ORDER BY DateTime ASC"),
        @NamedNativeQuery(name="ActivityHub.getHistoryCallByCallKeyDayAndCallKey",
                resultSetMapping="activityMapping",
                query="SELECT \n" +
                        "name = TCD.ANI,\n" +
                        "DateTime as datetime,\n" +
                        "TCD.WrapupData as content, \n" +
                        "CONCAT(Person.FirstName, ' ',Person.LastName) as agentName,\n" +
                        "agentId = Person.PersonID, \n" +
                        "source ='phone',\n" +
                        "duration = TCD.Duration,\n" +
                        "subject = '', \n" +
                        "type = 'inbound', \n" +
                        "conversationId = TCD.ANI\n" +
                        "FROM \n" +
                        "Termination_Call_Detail TCD JOIN\n" +
                        "Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\n" +
                        "Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\n" +
                        "LEFT JOIN Person ON Agent.PersonID = Person.PersonID\n" +
                        "WHERE\n" +
                        "Agent.EnterpriseName IS NOT NULL and TCD.RouterCallKeyDay = :callKeyDay  " +
                        "and TCD.RouterCallKey= :callKey and TCD.CallDispositionFlag = 1 AND ANI= :ani \n" +
                        "ORDER BY DateTime ASC"),
        @NamedNativeQuery(name="ActivityHub.getHistoryCallBetweenCallKeyIdAndLastCallKeyId",
                resultSetMapping="activityMapping",
                query="SELECT \n" +
                        "name = TCD.ANI,\n" +
                        "DateTime as datetime,\n" +
                        "TCD.WrapupData as content, \n" +
                        "CONCAT(Person.FirstName, ' ',Person.LastName) as agentName,\n" +
                        "agentId = Person.PersonID, \n" +
                        "source ='phone',\n" +
                        "duration = TCD.Duration,\n" +
                        "subject = '', \n" +
                        "type = 'inbound', \n" +
                        "conversationId = TCD.ANI\n" +
                        "FROM \n" +
                        "Termination_Call_Detail TCD JOIN\n" +
                        "Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\n" +
                        "Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\n" +
                        "LEFT JOIN Person ON Agent.PersonID = Person.PersonID\n" +
                        "WHERE\n" +
                        "Agent.EnterpriseName IS NOT NULL \n" +
                        "AND CONCAT(TCD.RouterCallKeyDay, '', TCD.RouterCallKey) >= :callKeyId AND \n"+
                        "CONCAT(TCD.RouterCallKeyDay, '', TCD.RouterCallKey) < :lastCallKeyId \n"+
                        "and TCD.CallDispositionFlag = 1 AND ANI= :ani \n" +
                        "ORDER BY DateTime ASC"),
        @NamedNativeQuery(name="ActivityHub.getHistoryCallByCallKeyIdGreatherThan",
                resultSetMapping="activityMapping",
                query="SELECT \n" +
                        "name = TCD.ANI,\n" +
                        "DateTime as datetime,\n" +
                        "TCD.WrapupData as content, \n" +
                        "CONCAT(Person.FirstName, ' ',Person.LastName) as agentName,\n" +
                        "agentId = Person.PersonID, \n" +
                        "source ='phone',\n" +
                        "duration = TCD.Duration,\n" +
                        "subject = '', \n" +
                        "type = 'inbound', \n" +
                        "conversationId = TCD.ANI\n" +
                        "FROM \n" +
                        "Termination_Call_Detail TCD JOIN\n" +
                        "Agent ON TCD.AgentSkillTargetID = Agent.SkillTargetID JOIN\n" +
                        "Skill_Group ON TCD.SkillGroupSkillTargetID = Skill_Group.SkillTargetID\n" +
                        "LEFT JOIN Person ON Agent.PersonID = Person.PersonID\n" +
                        "WHERE\n" +
                        "Agent.EnterpriseName IS NOT NULL \n" +
                        "AND CONCAT(TCD.RouterCallKeyDay, '', TCD.RouterCallKey) >= :callKeyId \n"+
                        "and TCD.CallDispositionFlag = 1 AND ANI= :ani \n" +
                        "ORDER BY DateTime ASC")
})

public class ActivityHub {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id;
    private String name;
    private String datetime;
    private String content;
    private String agentName;
    private String agentId;
    private String source;
    private Long duration;
    private String subject;
    private String type;
    private String conversationId;

    public ActivityHub(String name, String datetime, String content, String agentName, String agentId, String source, Long duration, String subject, String type, String conversationId) {
        this.name = name;
        this.datetime = datetime;
        this.content = content;
        this.agentName = agentName;
        this.agentId = agentId;
        this.source = source;
        this.duration = duration;
        this.subject = subject;
        this.type = type;
        this.conversationId = conversationId;
    }

    public ActivityHub(){
    }
}