package com.msi.cdx.model.internal;

import lombok.Data;

import java.util.List;

@Data
public class Journey {
    private List<Activity> activities;
    private Customer customer;
}
