package com.msi.cdx.model.internal;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Audited
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"customer_id", "phoneNumber"})
})
public class PhoneMapping {

    @GenericGenerator(
            name = "phoneMappingSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "phone_mapping_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "phoneMappingSequenceGenerator")
    @Column(name = "id")
    Long id;

    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    private String phoneNumber;

    private String callKeyDay;

    private String callKey;

    private String lastCallKeyDay;

    private String lastCallKey;

    @Column(name="started_at", updatable=false)
    Timestamp startedAt;

    @Column(name="created_at", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
    Timestamp createdAt;

    @NotNull
    String createdIdBy;
    @NotNull
    String createdNameBy;

    @Column(name="modified_at")
    Timestamp modifiedAt;

    @NotNull
    String modifiedIdBy;
    @NotNull
    String modifiedNameBy;


}
