package com.msi.cdx.model.internal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class Activity implements Comparable<Activity>{
    private String name;
    private String datetime;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date date;
    private String content;
    @JsonProperty("agent_name")
    private String agentName;
    @JsonProperty("agent_id")
    private String agentId;
    private String source;
    private Long duration;
    private String subject;
    private String type;
    @JsonProperty("conversation_id")
    private String conversationId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Date getDate() throws ParseException {
        log.info("getDate "+getSource() + source + this.source );
        log.info("getDate datetime "+this.datetime );
        if(this.source.equals("phone")){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       //     log.info("getDate date"+formatter.parse(this.datetime.substring(0,19)));
            return formatter.parse(this.datetime.substring(0,19));
        }
        else if(this.source.equals("email")){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            return formatter.parse(this.datetime);
        }else{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            return formatter.parse(this.datetime);
        }
    //    return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content == null ? "" : content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    @Override
    public int compareTo(Activity activity){
        try {
            if (getDate() == null || activity.getDate() == null) {
                return 0;
            }
            return getDate().compareTo(activity.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
