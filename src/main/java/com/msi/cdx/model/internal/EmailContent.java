package com.msi.cdx.model.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EmailContent {
    @JsonProperty("content_id")
    String contentId;
    String content;

}
