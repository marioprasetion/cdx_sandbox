package com.msi.cdx.model.internal;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@AllArgsConstructor
@Audited
@NoArgsConstructor
//added 28/6/2021
//@Table(schema = "cdx")

public class Customer {
    @GenericGenerator(
            name = "customerSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "customer_seq"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Id
    @GeneratedValue(generator = "customerSequenceGenerator")
    @Column(name = "id")
    Long id;

    @Column(unique=true, nullable = false)
    String phoneNumber;

    @Column(nullable = false)
    private String callKeyDay;

    @Column(nullable = false)
    private String callKey;

    @Column(name="created_at", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
    Timestamp createdAt;

    @Column(name="modified_at")
    Timestamp modifiedAt;

    @NotNull
    String createdIdBy;
    @NotNull
    String createdNameBy;
    @NotNull
    String modifiedIdBy;
    @NotNull
    String modifiedNameBy;

    @OneToMany(mappedBy = "customer")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<PhoneMapping> phoneMappingList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Timestamp modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getCreatedIdBy() {
        return createdIdBy;
    }

    public void setCreatedIdBy(String createdIdBy) {
        this.createdIdBy = createdIdBy;
    }

    public String getCreatedNameBy() {
        return createdNameBy;
    }

    public void setCreatedNameBy(String createdNameBy) {
        this.createdNameBy = createdNameBy;
    }

    public String getModifiedIdBy() {
        return modifiedIdBy;
    }

    public void setModifiedIdBy(String modifiedIdBy) {
        this.modifiedIdBy = modifiedIdBy;
    }

    public String getModifiedNameBy() {
        return modifiedNameBy;
    }

    public void setModifiedNameBy(String modifiedNameBy) {
        this.modifiedNameBy = modifiedNameBy;
    }

    @JsonManagedReference
    public Set<PhoneMapping> getPhoneMappingList() {
        return phoneMappingList;
    }

    public void setPhoneMappingList(Set<PhoneMapping> phoneMappingList) {
        this.phoneMappingList = phoneMappingList;
    }

    public String getCallKeyDay() {
        return callKeyDay;
    }

    public void setCallKeyDay(String callKeyDay) {
        this.callKeyDay = callKeyDay;
    }

    public String getCallKey() {
        return callKey;
    }

    public void setCallKey(String callKey) {
        this.callKey = callKey;
    }
}
