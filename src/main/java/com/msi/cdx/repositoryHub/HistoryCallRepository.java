package com.msi.cdx.repositoryHub;

import com.msi.cdx.model.pcce.HistoryCall;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface HistoryCallRepository extends CrudRepository<HistoryCall, String> {
    List<HistoryCall> getHistoryCall(@Param("ani") String ani);

}

