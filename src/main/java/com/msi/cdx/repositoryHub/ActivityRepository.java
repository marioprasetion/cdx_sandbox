package com.msi.cdx.repositoryHub;

import com.msi.cdx.model.pcce.ActivityHub;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ActivityRepository extends CrudRepository<ActivityHub, String> {
    List<ActivityHub> getHistoryCall(@Param("ani") String ani);
    List<ActivityHub> getHistoryCallBetweenCallKeyIdAndLastCallKeyId(@Param("ani") String ani, @Param("callKeyId") String callKeyId,
                                                          @Param("lastCallKeyId") String lastCallKeyId);
    List<ActivityHub> getHistoryCallByCallKeyIdGreatherThan(@Param("ani") String ani, @Param("callKeyId") String callKeyId);
    List<ActivityHub> getHistoryCallByCallKeyDayAndCallKey(@Param("ani") String ani,
                                                           @Param("callKeyDay") String callKeyDay,
                                                           @Param("callKey") String callKey);
}

